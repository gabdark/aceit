<?php get_header(); ?>

<section class="content container-fluid">

	<h1><?php _e('Blogue', 'theme'); ?></h1>
	
	<div class="row">

		<aside class="blog-sidebar col-md-3 col-sm-12 col-xs-12">
			<?php dynamic_sidebar( 'blog-sidebar' ); ?>
		</aside>

		<div class="post-list col-md-9 col-sm-12 col-xs-12">
			<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

			<article class="post">

				<?php if ( has_post_thumbnail() ) : ?>
				<figure class="post-thumbnail">
					<?php the_post_thumbnail('single-post-image'); ?>
				</figure>
				<?php endif; ?>

				<h2><?php the_title(); ?></h2>

				<aside class="meta">
					<span class="loop-date"><?php the_time('j F Y') ?></span>
					<span class="loop-author"><?php _e('Par', 'theme'); ?> <?php the_author_posts_link(); ?></span>
					<span class="loop-cats"><?php the_category(', '); ?></span>
				</aside>

				<?php the_content(); ?>

			</article>

			<?php endwhile; endif; ?>

		</div>

	</div>

</section>

<?php get_footer();