<div class="mobile-nav-wrap">
    <div class="top-mobile">
        <button class="hamburger mobile-nav-toggle c-hamburger--htx">
          <span>toggle menu</span>
        </button>
    </div>
    <div class="mobile-menu">
        <ul class="top-m">
            <?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'container' => false, 'items_wrap' => '%3$s' )); ?>
        </ul>
        <ul class="bot-m">
            <!-- <?php if( function_exists('icl_get_languages') ){
                $icl_langs = icl_get_languages('skip_missing=0&orderby=name&order=asc');
                foreach( $icl_langs as $lang ){
                    if( ! $lang['active'] ){
                        echo '<li><a class="language" href="' . $lang['url'] . '">' . strtoupper( $lang['language_code'] ) . '</a></li>';
                    }
                }
            } ?> -->
        </ul>
    </div>
</div>
