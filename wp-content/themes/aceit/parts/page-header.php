<?php
    $size = "normal";
    if( get_field('slider_size') == "small" ){
        $size = "small";
    }
?>
<?php if ( have_rows('header_slider') ) : ?>
    <div class="slider-wrap <?php echo $size; ?>">
        <div class="slider">
            <?php while ( have_rows('header_slider') ) : the_row(); ?>
                <?php
                    $image  = get_sub_field('bg');
                    $title  = get_sub_field('title');
                    $text   = get_sub_field('desc');
                ?>
                <?php if( $image ): ?>
                <div class="slide" style="background: url(<?php echo $image['url']; ?>); background-size: cover; background-position: center; background-repeat: no-repeat;">
                    <div class="container">
                        <div class="caption">
                            <div class="inner">
                                <?php if( $title ): ?>
                                    <h1><?php echo $title; ?></h1>
                                <?php endif; ?>
                                <?php if( $text ): ?>
                                    <p><?php echo $text; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>
