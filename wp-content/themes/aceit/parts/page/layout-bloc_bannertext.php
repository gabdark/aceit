<?php global $naj_functions;

$img 	   =  get_sub_field('img');
$content   = get_sub_field('text');
$button    = get_sub_field('button');
$link      = get_sub_field('link');

?>
<div class="bloc-cta">
    <div class="container">
        <div class="inner">
            <figure style="background-image:url(<?php echo $img['url']; ?>);"></figure>
            <div class="content">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
    <section class="cta-link">
        <a href="<?php echo $link; ?>"><?php echo $button; ?> <i class="fa fa-angle-right"></i></a>
    </section>
</div>
