<?php global $naj_functions;

$left   =  get_sub_field('half_blocks_left_block');
$right  = get_sub_field('half_blocks_right_block');

?>
<div class="halfblocks">
    <div class="flex-col">
        <div class="inner">
            <?php echo $left; ?>
        </div>
    </div>
    <div class="flex-col">
        <div class="inner">
            <?php echo $right; ?>
        </div>
    </div>
</div>
