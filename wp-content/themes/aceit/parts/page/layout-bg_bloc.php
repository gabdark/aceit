<?php global $naj_functions;

$bg		   = get_sub_field('bg');
$bg_options = get_sub_field('bg_options');
$color      = get_sub_field('color');
$cols       = get_sub_field('cols');

$count      = count( $cols );

switch ($count) {
   case 1: $column = "col-sm-12"; break;
   case 2: $column = "col-sm-6"; break;
   case 3: $column = "col-sm-4"; break;
   case 4: $column = "col-sm-3"; break;
}

if( $bg_options == "cover" ):
    $bg_option = "background-size:cover;";
elseif( $bg_options == "color" ):
    $bg_option = 'background-color:'.$color.';';
elseif( $bg_options == "repeat" ):
    $bg_option = "background-repeat:repeat;";
endif;

?>
<div class="bg-bloc" style="background-image:url(<?php echo $bg['url']; ?>); <?php echo $bg_option; ?>">
    <div class="container">
        <div class="row">
            <?php foreach( $cols as $col ): ?>
            <?php
               $title      = $col['title'];
               $titlec     = $col['title_color'];
               $content    = $col['content_bloc'];
               $c_color    = $col['content_color'];
               $t_align    = $col['t_align'];

               $add_but    = $col['add_button'];
               $button     = $col['button'];
               $link       = $col['link'];

             ?>
             <div class="<?php echo $column; ?>">
                <div class="bloc-c">
                    <?php if( $title ): ?>
                        <h2 class="<?php echo $t_align; ?>" style="color:<?php echo $titlec; ?>"><?php echo $title; ?></h2>
                    <?php endif; ?>
                    <div class="content" <?php if( $c_color ): ?>style="color: <?php echo $c_color; ?>"<?php endif; ?>>
                        <?php echo $content; ?>
                    </div>

                    <?php if( $add_but == true ): ?>
                        <a href="<?php echo $link; ?>" class="button-cta <?php echo $t_align; ?>"><?php echo $button; ?> <i class="fa fa-angle-right"></i></a>
                    <?php endif; ?>
                </div>
             </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
