<?php global $naj_functions;

    $title      = get_sub_field('white_block_title');
    $content    = get_sub_field('white_block_content');
    $add        = get_sub_field('add_image');
    $pos        = get_sub_field('image_position');
    $img        = get_sub_field('white_block_img');

    if( $add == true ){
        $class = "left-pos figure-in";

        if( $pos == "right" ){
            $class = "right-pos figure-in";
        }
    }
?>
<div class="white-block">
    <div class="flex-col <?php echo $class; ?>">
        <div class="text-c">
            <div class="inner">
                <h3><?php echo $title; ?></h3>
                <?php echo $content; ?>
            </div>
        </div>
        <?php if( $add ): ?>
            <figure class="white-block-img" style="background-image:url(<?php echo $img['url']; ?>);"></figure>
        <?php endif; ?>
    </div>
</div>
