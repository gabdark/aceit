<?php global $naj_functions;

$news_bg        = get_field('news_bg', 'options');
$news_title     = get_field('news_title', 'options');
$news_subtitle  = get_field('news_subtitle', 'options');


?>
<div class="newsletter-section" style="background-image:url(<?php echo $news_bg['url']; ?>);">
    <div class="container">
        <h2><?php echo $news_title; ?></h2>
        <h4><?php echo $news_subtitle; ?></h4>
        <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
    </div>
</div>
