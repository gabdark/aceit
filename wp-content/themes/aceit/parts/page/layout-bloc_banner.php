<?php global $naj_functions;

$img 	   =  get_sub_field('img');
$content   = get_sub_field('content_bloc');

?>
<div class="bloc-banner">
    <div class="container">
        <figure>
            <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>" />
        </figure>
        <div class="content">
            <?php echo $content; ?>
        </div>
    </div>
</div>
