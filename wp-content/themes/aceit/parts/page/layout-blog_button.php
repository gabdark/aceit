<?php global $naj_functions;

$bg		   = get_sub_field('bg');
$bg_options = get_sub_field('bg_options');
$color      = get_sub_field('color');
$cols       = get_sub_field('blocs');

$count      = count( $cols );

switch ($count) {
   case 1: $column = "col-sm-12"; break;
   case 2: $column = "col-sm-6"; break;
   case 3: $column = "col-sm-4"; break;
   case 4: $column = "col-sm-3"; break;
}

if( $bg_options == "cover" ):
    $bg_option = "background-size:cover;";
elseif( $bg_options == "color" ):
    $bg_option = 'background-color:'.$color.';';
elseif( $bg_options == "repeat" ):
    $bg_option = "background-repeat:repeat;";
endif;

?>
<div class="blocs-buttons" style="background-image:url(<?php echo $bg['url']; ?>); <?php echo $bg_option; ?>">
    <div class="container">
        <div class="row">
            <?php foreach( $cols as $col ): ?>
            <?php
               $img     = $col['bloc_img'];
               $button  = $col['button'];
               $b_color = $col['button_color'];
               $t_color = $col['text_color'];
               $link    = $col['link'];
             ?>
             <div class="<?php echo $column; ?>">
                <figure class="bloc-bg" style="background-image:url(<?php echo $img['url']; ?>);">
                   <?php if( $button ): ?>
                       <a href="<?php echo $link; ?>" class="bg-button" style="background-color:<?php echo $b_color; ?>; color: <?php echo $t_color; ?>; "><?php echo $button; ?></a>
                   <?php endif; ?>
                </figure>
             </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
