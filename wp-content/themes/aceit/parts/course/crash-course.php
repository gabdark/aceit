<?php

    global $naj_functions;
    $crashes	= get_field('crash_courses');

?>
<?php if( $crashes ): ?>
    <div class="crashes">
        <?php foreach ( $crashes as $weekly ): ?>
            <?php
                $id			= str_replace(' ', '', strtolower($weekly['group_name']));
                $name		= $weekly['group_name'];
                $tutor		= $weekly['tutor'];
                $price		= $weekly['price'];
                $schedules	= $weekly['schedule'];
                $form_data	= get_the_title() . ' - ' . $weekly['group_name'];
            ?>
            <div class="crash white-block form-entry-data" data-weekly="<?php echo $id; ?>" data-form="<?php echo $form_data; ?>">

                <div class="row">
                    <div class="col-sm-6">
                        <h3><?php echo $name; ?></h3>
                        <h5><?php _e('Tutor'); ?>: <span><?php echo $tutor; ?></span></h5>
                        <h5><?php _e('Price'); ?>: <span><?php echo $price; ?></span></h5>

                        <?php if( $schedules ): ?>

                            <h5 class="schedule-title"><?php _e('Schedule'); ?>:</h5>
                            <div class="schedules">
                                <?php foreach ( $schedules as $schedule ): ?>
                                    <?php
                                        $session		= $schedule['session'];
                                        $chapter		= $schedule['chapter'];
                                        $date_time		= $schedule['date_time'];
                                    ?>
                                    <div class="schedule">
                                        <h6><?php echo $session; ?> <span><?php echo $chapter; ?></span></h6>
                                        <p><?php echo $date_time; ?></p>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                        <?php endif; ?>
                    </div>
                    <div class="col-sm-6">
                        <?php echo do_shortcode('[gravityform id="3" title="true" description="false" ajax="false"]'); ?>
                    </div>
                </div>

            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
