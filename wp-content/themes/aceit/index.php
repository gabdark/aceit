<?php get_header(); ?>

<section class="content container-fluid">

	<h1><?php _e('Blogue', 'theme'); ?></h1>

	<?php if ( is_category() ) :?>
		<h2><?php single_cat_title(); ?></h2>
	<?php elseif ( is_archive() ) : ?>
		<h2><?php single_month_title(' '); ?></h2>
	<?php endif; ?>

	<div class="row">

		<aside class="blog-sidebar col-md-3 col-sm-12 col-xs-12">
			<?php dynamic_sidebar( 'blog-sidebar' ); ?>
		</aside>

		<div class="post-list col-md-9 col-sm-12 col-xs-12">
			<?php if( have_posts() ) : while( have_posts() ) : the_post(); ?>

			<article class="post row">

				<?php if ( has_post_thumbnail() ) : ?>
				<figure class="post-thumbnail col-md-5 col-sm-5 col-xs-12">
					<a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('post-list-thumbnail'); ?></a>
				</figure>
				<?php endif; ?>

				<?php $classes = 'col-md-12';

				if ( has_post_thumbnail() ) $classes = 'col-md-7 col-sm-7 col-xs-12'; ?>

				<div class="<?php echo $classes; ?>">
					<h3><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>

					<aside class="meta">
						<span class="loop-date"><i class="fa fa-calendar"></i> <?php the_time('j F Y') ?></span>
						<span class="loop-author"><i class="fa fa-user"></i> <?php _e('Par', 'theme'); ?> <?php the_author_posts_link(); ?></span>
						<span class="loop-cats"><i class="fa fa-tags"></i> <?php the_category(', '); ?></span>
					</aside>

					<?php the_excerpt(); ?>
					<a href="<?php echo the_permalink(); ?>"><?php _e('Lire la suite', 'theme'); ?></a>
				</div>

			</article>

			<?php endwhile; endif; ?>

			<div class="pagination"><?php $naj_functions->archive_pagination(); ?></div>

		</div>

	</div>

</section>

<?php get_footer();
