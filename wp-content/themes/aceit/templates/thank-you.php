<?php /* Template Name: Page: Thank you */ get_header(); ?>

<?php

	$section_name = str_replace('+', ' ', $_SERVER["QUERY_STRING"]);
	$last_page = $_SERVER['HTTP_REFERER'];
?>

<?php if ( have_rows('header_slider') ) : ?>
    <div class="slider-wrap">
        <div class="slider">
            <?php while ( have_rows('header_slider') ) : the_row(); ?>
                <?php
                    $image  = get_sub_field('bg');
                    $title  = get_sub_field('title');
                    $text   = get_sub_field('desc');
                ?>
                <?php if( $image ): ?>
                <div class="slide" style="background: url(<?php echo $image['url']; ?>); background-size: cover; background-position: center; background-repeat: no-repeat;">
                    <div class="container">
                        <div class="caption">
                            <div class="inner">
                                <?php if( $title ): ?>
                                    <h1><?php _e('Request sent for '); ?><?php echo $section_name; ?></h1>
                                <?php endif; ?>
                                <?php if( $text ): ?>
                                    <p><?php echo $text; ?></p>
									<a href="<?php echo $last_page; ?>" class="button-cta"><?php _e('Go back to the course'); ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>

<?php get_footer();
