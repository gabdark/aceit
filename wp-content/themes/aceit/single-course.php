<?php get_header(); ?>

<?php

	global $wp_query;
	$weeklies	= get_field('weeklies_groups');
	$crashes	= get_field('crash_courses');
	$mocks		= get_field('mock_exams');

	$c_week = "";
	$c_crash = "";
	$c_mock = "";

	$thumb = $naj_functions->imgURL('single-fallback');

	if ( has_post_thumbnail() ){
		$thumb = get_the_post_thumbnail_url();
	}

	$arr_weeklies = array_key_exists( 'weeklies', $wp_query->query_vars );
	$arr_crashes  = array_key_exists( 'crash-course', $wp_query->query_vars );
	$arr_mocks	  = array_key_exists( 'mock-exams', $wp_query->query_vars );
?>

<div class="single-header" style="background-image:url(<?php echo $thumb; ?>);">
	<div class="caption">
		<div class="inner">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
</div>

<nav class="course-nav">

	<?php
		if( !$arr_weeklies && !$arr_crashes && !$arr_mocks ):

			if( $weeklies ):
				$c_week = "active";
			elseif( $crashes ):
				$c_crash = "active";
			elseif( $mocks ):
				$c_mock = "active";
			endif;

		endif;
	?>

	<div class="inner">
		<a href="<?php echo get_permalink(155); ?>">
			<?php _e('Private tutoring'); ?>
		</a>
		<?php if( $weeklies ): ?>
			<a href="<?php echo get_permalink(); ?>weeklies" class="<?php echo $c_week; ?> <?php if( $arr_crashes ): ?>active<?php endif;?>">
				<?php _e('Weeklies'); ?>
			</a>
		<?php endif; ?>
		<?php if( $crashes ): ?>
			<a href="<?php echo get_permalink(); ?>crash-course" class="<?php echo $c_crash; ?> <?php if( $arr_crashes ): ?>active<?php endif;?>">
				<?php _e('Crash courses'); ?>
			</a>
		<?php endif; ?>
		<?php if( $mocks ): ?>
			<a href="<?php echo get_permalink(); ?>mock-exams" class="<?php echo $c_mock; ?> <?php if( $arr_mocks ): ?>active<?php endif;?>">
				<?php _e('Mock exams'); ?>
			</a>
		<?php endif; ?>
	</div>
</nav>

<section class="content" id="course-content">

	<?php if( !$arr_weeklies && !$arr_crashes && !$arr_mocks ): ?>

		<?php if( $weeklies ): ?>
			<section id="weeklies" class="section">
				<?php get_template_part('parts/course/weekly'); ?>
			</section>
		<?php elseif( $crashes ): ?>
			<section id="crash-course" class="section">
				<?php get_template_part('parts/course/crash-course'); ?>
			</section>
		<?php elseif( $mocks ): ?>
			<section id="mock-exams" class="section">
				<?php get_template_part('parts/course/mock-exams'); ?>
			</section>
		<?php endif; ?>

	<?php endif; ?>

	<?php if( $arr_weeklies ): ?>
		<section id="weeklies" class="section">
			<?php get_template_part('parts/course/weekly'); ?>
		</section>
	<?php endif; ?>

	<?php if( $arr_crashes ): ?>
		<section id="crash-course" class="section">
			<?php get_template_part('parts/course/crash-course'); ?>
		</section>
	<?php endif; ?>

	<?php if( $arr_mocks ): ?>
		<section id="mock-exams" class="section">
			<?php get_template_part('parts/course/mock-exams'); ?>
		</section>
	<?php endif; ?>

</section>

<?php get_template_part('parts/page/layout-newsletter_block'); ?>

<?php get_footer();
