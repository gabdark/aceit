<?php global $naj_functions; ?>
		</div>

		<footer class="footer">
			<div class="container">
				<div class="white-block">
					<?php echo do_shortcode('[gravityform id="1" title="true" description="false" ajax="true"]'); ?>
				</div>
			</div>
			<div class="copyright">
				<div class="container">
					<span>© <?php echo date('Y'); ?> Ace it Tutorials</span>
					<a href="http://najomie.com">Web design by Najomie.com</a>
				</div>
			</div>
		</footer>

	</div>

	<?php
		global $post;
		$args		= array( 'post_type' => 'course' , 'posts_per_page' => -1, 'order'=> 'ASC', 'orderby' => 'menu_order' );
		$courses	= get_posts( $args );
	?>
	<div id="courses">
		<?php foreach ( $courses as $post ) : setup_postdata( $post ); ?>
			<div class="menu-item">
				<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?> <span>></span></a>
				<ul class="sub-sub">
					<a href="<?php echo get_permalink(155); ?>">
						<?php _e('Private tutoring'); ?>
					</a>
					<a href="<?php echo get_permalink(); ?>weeklies">
						<?php _e('Weeklies'); ?>
					</a>
					<a href="<?php echo get_permalink(); ?>crash-course" >
						<?php _e('Crash courses'); ?>
					</a>
					<a href="<?php echo get_permalink(); ?>mock-exams">
						<?php _e('Mock exams'); ?>
					</a>
				</ul>
			</div>
		<?php endforeach; wp_reset_postdata(); ?>
	</div>
	<?php wp_reset_postdata(); ?>

	<?php wp_footer(); ?>
</body>
</html>
