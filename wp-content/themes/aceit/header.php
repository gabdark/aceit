<?php global $naj_functions;
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html id="top" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="dns-prefetch" href="//fonts.googleapis.com/">

	<title><?php wp_title(); ?></title>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="page-wrapper">

		<header class="header">
			<div class="container">
				<a class="logo" href="<?php echo site_url() ?>"><span class="tagline">Ace it <strong>Tutorials</strong></span></a>
				<?php wp_nav_menu( array( 'theme_location' => 'main-nav', 'container' => 'nav', 'container_class' => 'main-nav-wrap', 'fallback_cb' => 'false' )); ?>

				<?php get_template_part('parts/mobile-menu'); ?>
			</div>
		</header>

		<div class="main">
