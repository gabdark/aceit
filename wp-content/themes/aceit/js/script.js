jQuery(document).ready(function($) {

	$('#wpadminbar').addClass('Fixed');
	$("#courses").appendTo(".sub-menu");

	// Mobile menu
	$('.hamburger').click(function(){
		$(this).toggleClass('is-active');
		$('.mobile-menu').slideToggle(300);
		return false;
	});

	$('.menu .menu-item-has-children > a').click(function(e){
		return false;
	});

	$('.menu .menu-item-has-children').hover(function(e){
		$(this).toggleClass('menu-active');
	});

	$('#courses .menu-item').hover(function(e){
		$(this).toggleClass('sub-active');
	});


	// $('.course-nav a').click(function(){
	// 	$(".course-nav a:not(this)").removeClass("active");
	// 	$(this).toggleClass('active');
	//
	// 	var elem = $("#course-content" + id);
	// 	elem.toggleClass('active-section');
	// 	return false;
	// });


	// Slick Slider
	if ( $.fn.slick ) {

		var options = {
		  dots: true,
		  arrows: false,
		  pauseOnHover: false,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  fade: false,
		  draggable: true,
		  swipe: true,
		  autoplay: true,
		  speed: 1500,
		  autoplaySpeed: 66000
	  };

		$('.slider').slick(options);
	}

	$('#gform_wrapper_3 .gform_button').click(function(e) {
		formdata = $(this).closest('.form-entry-data').data('form');
		$('#gform_wrapper_3 #input_3_5').val( formdata );
	});

});
