<?php
/**
 * Plugin Name: Aceit Courses
 * Plugin URI: aceittutorials.com
 * Description: Plugins for courses
 * Version: 1.0.8
 * Author: Gabriel Gaudreau
 * Author URI: http://najomie.com
 * Requires at least: 4.1
 * Tested up to: 4.1
 *
 * @package NAJ
 * @category Core
 * @author Gabriel Gaudreau
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'ACE_Course' ) ) {

	class ACE_Course
	{
		const DEBUG = FALSE;

		/**
		 * Plugin version, match with plugin header
		 * @var string
		 */
		public $version = '1.0.0';

		/**
		 * Use the function not the variable
		 * @var string
		 */
		public $plugin_url;

		/**
		 * Use the function not the variable
		 * @var string
		 */
		public $plugin_path;

		/**
		 * Do we update the rewrite rules for a custom post type?
		 * @var boolean
		 */
		public $flush_rules = FALSE;

		/**
		 * PLUGIN STARTUP
		 */
		public function __construct(){
			// do something when we activate/deactivate the plugin
			register_activation_hook( __FILE__, array( $this, 'activate' ) );
			register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

			$installed = get_option( __CLASS__ . '_Version', FALSE );

			if( ! $installed or version_compare($installed, $this->version, '!=') ){
				add_action( 'init', array($this, 'activate'), 9 );
				update_option( __CLASS__ . '_Version', $this->version );
			}

			$this->hooks();
		}

		/**
		 * Register the plugin's hooks
		 */
		public function hooks(){
			add_action( 'init', array($this, 'init'), 0 );
		}

		/**
		 * Runs on WordPress init hook
		 */
		public function init(){
			$this->post_types();
			$this->flush();
		}
		/**
		 * Register the post types and taxonomies
		 *
		 * @link http://fooplugins.com/generators/wordpress-custom-post-types/
		 * @link http://fooplugins.com/generators/wordpress-custom-taxonomy/ (OFFLINE)
		 * @link https://developer.wordpress.org/resource/dashicons/ for admin menu icons
		 */
		 // Register Custom Post Type
		 public function post_types() {

			$labels = array(
				'name'                  => _x( 'Courses', 'Post Type General Name', 'aceit' ),
				'singular_name'         => _x( 'Course', 'Post Type Singular Name', 'aceit' ),
				'menu_name'             => __( 'Courses', 'aceit' ),
				'name_admin_bar'        => __( 'Course', 'aceit' ),
				'archives'              => __( 'Course Archives', 'aceit' ),
				'parent_item_colon'     => __( 'Course parent:', 'aceit' ),
				'all_items'             => __( 'All courses', 'aceit' ),
				'add_new_item'          => __( 'Add new course', 'aceit' ),
				'not_found_in_trash'    => __( 'Not found in Trash', 'aceit' ),
				'set_featured_image'    => __( 'Set featured image', 'aceit' ),
				'remove_featured_image' => __( 'Remove featured image', 'aceit' ),
				'use_featured_image'    => __( 'Use as featured image', 'aceit' ),
				'insert_into_item'      => __( 'Insert into item', 'aceit' ),
				'uploaded_to_this_item' => __( 'Uploaded to this item', 'aceit' ),
				'items_list'            => __( 'Items list', 'aceit' ),
				'items_list_navigation' => __( 'Items list navigation', 'aceit' ),
				'filter_items_list'     => __( 'Filter items list', 'aceit' ),
			);
			$args = array(
				'label'                 => __( 'Course', 'aceit' ),
				'description'           => __( 'Courses', 'aceit' ),
				'labels'                => $labels,
				'supports'              => array( 'title', 'thumbnail', ),
				'hierarchical'          => false,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => true,
				'menu_icon'           	=> 'dashicons-welcome-learn-more',
				'menu_position'         => 5,
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => true,
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'rewrite'             => array(
					'slug'       => 'course',
					'with_front' => FALSE,
					'feeds'      => TRUE,
					'pages'      => TRUE
				),
				'capability_type'       => 'page',
			);
			register_post_type( 'course', $args );
		 }

		/**
		 * Refresh rewrite rules
		 */
		public function flush(){
			if( $this->flush_rules )
				flush_rewrite_rules();
		}

		public function activate(){
			$this->flush_rules = TRUE; // we will need to refresh the rewrite rules for the custom post types
		}

		public function deactivate(){
			$this->flush_rules = TRUE; // refresh the rewrite rules for the custom post types which are no longuer loaded
		}

		public function imgURL( $file ){
			return $this->plugin_url() . "/assets/images/{$file}";
		}

		public function jsURL( $file ){
			return $this->plugin_url() . "/assets/js/{$file}";
		}
		public function jsPATH( $file ){
			return $this->plugin_path() . "/assets/js/{$file}";
		}

		public function cssURL( $file ){
			return $this->plugin_url() . "/assets/css/{$file}";
		}
		public function cssPATH( $file ){
			return $this->plugin_path() . "/assets/css/{$file}";
		}

		/**
		 * Get the plugin url.
		 *
		 * @access public
		 * @return string
		 */
		public function plugin_url() {
			if ( $this->plugin_url ) return $this->plugin_url;
			return $this->plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
		}

		/**
		 * Get the plugin path.
		 *
		 * @access public
		 * @return string
		 */
		public function plugin_path() {
			if ( $this->plugin_path ) return $this->plugin_path;
			return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
		}

		public function recent_tuts(){
			$recent_tuts = wp_get_recent_posts(array('post_type'=>'tutorial', 'numberposts'=>5 )); ?>
			<div class="widget">
				<h3><?php _e('Courses récents'); ?></h3>
				<ul>
					<?php foreach ($recent_tuts as $post): ?>
						<li><a href="<?php echo get_permalink($post["ID"]) ?>"><?php echo $post["post_title"]; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div><?php
		}

		public function list_cat(){
			$taxonomy = 'Course_cat'; $tax_terms = get_terms($taxonomy); ?>
			<div class="widget">
				<h3><?php _e('Catégories'); ?></h3>
				<ul>
					<?php foreach ($tax_terms as $tax_term): ?>
						<li><a href="<?php echo esc_attr(get_term_link($tax_term, $taxonomy)); ?>"><?php echo $tax_term->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div><?php
		}

		public function list_tag(){
			$taxonomy = 'Course_tag'; $tax_terms = get_terms($taxonomy); ?>
			<div class="widget">
				<h3><?php _e('Mots Clés'); ?></h3>
				<ul>
					<?php foreach ($tax_terms as $tax_term): ?>
						<li><a href="<?php echo esc_attr(get_term_link($tax_term, $taxonomy)); ?>"><?php echo $tax_term->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</div><?php
		}
	}

	// Init Class and register in global scope
	$GLOBALS['ACE_Course'] = new ACE_Course();

	} // class_exists check
